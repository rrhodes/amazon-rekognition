#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import AmazonRekognitionStack from '../lib/amazon-rekognition-stack';

const app = new App();
new AmazonRekognitionStack(app, 'AmazonRekognitionStack');
